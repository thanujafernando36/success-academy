﻿using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Dtos
{
    public class SubjectDto
    {
        [Required]
        public string Name { get; set; }
    }
}
