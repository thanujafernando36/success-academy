﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Dtos
{
    public class VideoDto
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public int Grade { get; set; }
        [Required]
        public string Link { get; set; }
        [Required]
        public int ClassId { get; set; }
        [Required]
        public string SubjectId { get; set; }
    }
}