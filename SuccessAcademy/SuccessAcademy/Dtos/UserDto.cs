﻿using System;

namespace SuccessAcademy.Dtos
{
    public class UserDto
    {
        public int Grade { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public string UserName { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public string ProfilePic { get; set; }
    }
}
