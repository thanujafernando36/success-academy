﻿using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Dtos
{
    public class ClassRoomDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Grade { get; set; }

        [Required]
        public string Language { get; set; }

        [Required]
        public string StartTime { get; set; }

        [Required]
        public string EndTime { get; set; }

        [Required]
        public int SubjectId { get; set; }
    }
}
