﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;

namespace SuccessAcademy.GlobalMethods
{
    public static class CustomHttpResponse
    {
        public static HttpResponseMessage invalidRequestResponse()
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                ReasonPhrase = "Invalid Request"
            };
            return response;
        }

        public static HttpResponseMessage objectNotFoundResponse(string missingObjectName)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                ReasonPhrase = string.Format("{0} not found", missingObjectName)
            };
            return response;
        }

        public static HttpResponseMessage errorOccurredResponse()
        {
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = "An error occurred"
            };
            return response;
        }

        public static HttpResponseMessage internalServerErrorResponse(string methodAction)
        {
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = String.Format("An error occured while {0}", methodAction)
            };
            return response;
        }

        public static HttpResponseMessage unauthorizedErrorResponse()
        {
            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
            {
                ReasonPhrase = "Invalid Login"
            };
            return response;
        }

        public static HttpResponseMessage okResponse(string content) 
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                ReasonPhrase = content
            };
            return response;
            //return response;
        }
    }
}
