﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuccessAcademy.Migrations
{
    public partial class videoRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassRoomId",
                table: "Video",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Video",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Video_ClassRoomId",
                table: "Video",
                column: "ClassRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Video_SubjectId",
                table: "Video",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Video_ClassRooms_ClassRoomId",
                table: "Video",
                column: "ClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Video_ClassRooms_ClassRoomId",
                table: "Video");

            migrationBuilder.DropForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video");

            migrationBuilder.DropIndex(
                name: "IX_Video_ClassRoomId",
                table: "Video");

            migrationBuilder.DropIndex(
                name: "IX_Video_SubjectId",
                table: "Video");

            migrationBuilder.DropColumn(
                name: "ClassRoomId",
                table: "Video");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Video");
        }
    }
}
