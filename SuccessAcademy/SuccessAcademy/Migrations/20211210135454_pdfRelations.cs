﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuccessAcademy.Migrations
{
    public partial class pdfRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassRoomId",
                table: "Pdfs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Pdfs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pdfs_ClassRoomId",
                table: "Pdfs",
                column: "ClassRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Pdfs_SubjectId",
                table: "Pdfs",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pdfs_ClassRooms_ClassRoomId",
                table: "Pdfs",
                column: "ClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pdfs_Subject_SubjectId",
                table: "Pdfs",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pdfs_ClassRooms_ClassRoomId",
                table: "Pdfs");

            migrationBuilder.DropForeignKey(
                name: "FK_Pdfs_Subject_SubjectId",
                table: "Pdfs");

            migrationBuilder.DropIndex(
                name: "IX_Pdfs_ClassRoomId",
                table: "Pdfs");

            migrationBuilder.DropIndex(
                name: "IX_Pdfs_SubjectId",
                table: "Pdfs");

            migrationBuilder.DropColumn(
                name: "ClassRoomId",
                table: "Pdfs");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Pdfs");
        }
    }
}
