﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuccessAcademy.Migrations
{
    public partial class dbsetRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassRooms_Subject_SubjectId",
                table: "ClassRooms");

            migrationBuilder.DropForeignKey(
                name: "FK_Pdfs_Subject_SubjectId",
                table: "Pdfs");

            migrationBuilder.DropForeignKey(
                name: "FK_Video_ClassRooms_ClassRoomId",
                table: "Video");

            migrationBuilder.DropForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Video",
                table: "Video");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subject",
                table: "Subject");

            migrationBuilder.RenameTable(
                name: "Video",
                newName: "Videos");

            migrationBuilder.RenameTable(
                name: "Subject",
                newName: "Subjects");

            migrationBuilder.RenameIndex(
                name: "IX_Video_SubjectId",
                table: "Videos",
                newName: "IX_Videos_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Video_ClassRoomId",
                table: "Videos",
                newName: "IX_Videos_ClassRoomId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Videos",
                table: "Videos",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassRooms_Subjects_SubjectId",
                table: "ClassRooms",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pdfs_Subjects_SubjectId",
                table: "Pdfs",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_ClassRooms_ClassRoomId",
                table: "Videos",
                column: "ClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Subjects_SubjectId",
                table: "Videos",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassRooms_Subjects_SubjectId",
                table: "ClassRooms");

            migrationBuilder.DropForeignKey(
                name: "FK_Pdfs_Subjects_SubjectId",
                table: "Pdfs");

            migrationBuilder.DropForeignKey(
                name: "FK_Videos_ClassRooms_ClassRoomId",
                table: "Videos");

            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Subjects_SubjectId",
                table: "Videos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Videos",
                table: "Videos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects");

            migrationBuilder.RenameTable(
                name: "Videos",
                newName: "Video");

            migrationBuilder.RenameTable(
                name: "Subjects",
                newName: "Subject");

            migrationBuilder.RenameIndex(
                name: "IX_Videos_SubjectId",
                table: "Video",
                newName: "IX_Video_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Videos_ClassRoomId",
                table: "Video",
                newName: "IX_Video_ClassRoomId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Video",
                table: "Video",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subject",
                table: "Subject",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassRooms_Subject_SubjectId",
                table: "ClassRooms",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pdfs_Subject_SubjectId",
                table: "Pdfs",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Video_ClassRooms_ClassRoomId",
                table: "Video",
                column: "ClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
