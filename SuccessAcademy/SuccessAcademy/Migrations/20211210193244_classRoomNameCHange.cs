﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuccessAcademy.Migrations
{
    public partial class classRoomNameCHange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentClass_ClassRooms_ClassRoomId",
                table: "StudentClass");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass");

            migrationBuilder.DropIndex(
                name: "IX_StudentClass_ClassRoomId",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "ClassRoomId",
                table: "StudentClass");

            migrationBuilder.AddColumn<int>(
                name: "StudentClassRoomId",
                table: "StudentClass",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass",
                columns: new[] { "UserId", "StudentClassRoomId" });

            migrationBuilder.CreateIndex(
                name: "IX_StudentClass_StudentClassRoomId",
                table: "StudentClass",
                column: "StudentClassRoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClass_ClassRooms_StudentClassRoomId",
                table: "StudentClass",
                column: "StudentClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentClass_ClassRooms_StudentClassRoomId",
                table: "StudentClass");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass");

            migrationBuilder.DropIndex(
                name: "IX_StudentClass_StudentClassRoomId",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "StudentClassRoomId",
                table: "StudentClass");

            migrationBuilder.AddColumn<int>(
                name: "ClassRoomId",
                table: "StudentClass",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass",
                columns: new[] { "UserId", "ClassRoomId" });

            migrationBuilder.CreateIndex(
                name: "IX_StudentClass_ClassRoomId",
                table: "StudentClass",
                column: "ClassRoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClass_ClassRooms_ClassRoomId",
                table: "StudentClass",
                column: "ClassRoomId",
                principalTable: "ClassRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
