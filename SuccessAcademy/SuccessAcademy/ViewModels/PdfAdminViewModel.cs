﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.ViewModels
{
    public class PdfAdminViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public int Grade { get; set; }
        [Required]
        public string Link { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }
        [Required]
        public string ClassRoomName { get; set; }
        [Required]
        public string SubjectName { get; set; }
    }
}