﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.ViewModels
{
    public class ClassRoomViewmodel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Grade { get; set; }

        public string Language { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string SubjectName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}
