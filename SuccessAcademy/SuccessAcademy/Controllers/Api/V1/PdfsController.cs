﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuccessAcademy.Authentication;
using SuccessAcademy.Dtos;
using SuccessAcademy.GlobalMethods;
using SuccessAcademy.Models;
using SuccessAcademy.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PdfsController : ControllerBase
    {
        private readonly AcademyContext _context;

        public PdfsController(AcademyContext context)
        {
            _context = context;
        }

        private PdfViewmodel MapPdfViewModel(Pdf pdf)
        {
            if (pdf.ClassRoom == null || pdf.Subject == null)
                throw new Exception();
            var pdfViewmodel = new PdfViewmodel
            {
                Id = pdf.Id,
                Name = pdf.Name,
                CreatedDate = pdf.CreatedDate,
                Description = pdf.Description,
                ClassRoomName = pdf.ClassRoom.Name,
                SubjectName = pdf.Subject.Name,
                Link = pdf.Link,
                Grade = pdf.ClassRoom.Grade,
                LastUpdated = pdf.LastUpdated
            };
            return pdfViewmodel;
        }

        private PdfAdminViewModel MapPdfAdminViewModel(Pdf pdf)
        {
            if (pdf.ClassRoom == null || pdf.Subject == null)
                throw new Exception();
            var pdfAdminViewModel = new PdfAdminViewModel
            {
                Id = pdf.Id,
                Name = pdf.Name,
                CreatedDate = pdf.CreatedDate,
                Description = pdf.Description,
                ClassRoomName = pdf.ClassRoom.Name,
                SubjectName = pdf.Subject.Name,
                Link = pdf.Link,
                Grade = pdf.ClassRoom.Grade,
                LastUpdated = pdf.LastUpdated
            };
            return pdfAdminViewModel;
        }

        // GET: api/Pdfs
        [HttpGet]
        public async Task<IActionResult> GetPdfs()
        {
            try
            {
                var pdfs = await _context.Pdfs.ToListAsync();
                if (pdfs == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdfs"));
                var pdfViewmodels = new List<PdfViewmodel>();
                foreach (Pdf pdf in pdfs)
                {
                    var pdfViewmodel = MapPdfViewModel(pdf);
                    pdfViewmodels.Add(pdfViewmodel);
                }
                return Ok(pdfViewmodels);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving pdfs"));
            }
        }

        // GET: api/Pdfs
        [Authorize(Roles = UserRoles.Admin)]
        [HttpGet("admin")]
        public async Task<IActionResult> GetPdfsForAdmin()
        {
            try
            {
                var pdfs = await _context.Pdfs.ToListAsync();
                if (pdfs == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdfs"));
                var pdfAdminViewModels = new List<PdfAdminViewModel>();
                foreach (Pdf pdf in pdfs)
                {
                    var pdfAdminViewModel = MapPdfAdminViewModel(pdf);
                    pdfAdminViewModels.Add(pdfAdminViewModel);
                }
                return Ok(pdfAdminViewModels);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving pdf for admin"));
            }
        }

        // GET: api/Pdfs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPdf(int id)
        {
            try
            {
                var pdf = await _context.Pdfs.FindAsync(id);
                if (pdf == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdf"));
                var pdfViewmodel = MapPdfViewModel(pdf);
                return Ok(pdfViewmodel);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving pdf"));
            }
        }

        // GET: api/Pdfs/admin/5
        [Authorize(Roles = UserRoles.Admin)]
        [HttpGet("admin/{id}")]
        public async Task<IActionResult> GetPdfForAdmin(int id)
        {
            try
            {
                var pdf = await _context.Pdfs.FindAsync(id);
                if (pdf == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdf"));
                var pdfViewmodel = MapPdfViewModel(pdf);
                return Ok(pdfViewmodel);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving pdfs for admin"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreatePdf([FromBody] PdfDto pdfDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating pdf"));
                var pdfClass = _context.ClassRooms.Find(pdfDto.ClassId);
                var pdfSubject = _context.Subjects.Find(pdfDto.SubjectId);
                var pdf = new Pdf
                {
                    Name = pdfDto.Name,
                    Description = pdfDto.Description,
                    Link = pdfDto.Link,
                    LastUpdated = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    ClassRoom = pdfClass,
                    Subject = pdfSubject
                };
                _context.Pdfs.Add(pdf);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("creating pdf"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPdf(int id, [FromBody] PdfDto pdfDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating pdf"));
                var pdf = await _context.Pdfs.FindAsync(id);
                if (pdf == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdf"));
                pdf.Link = pdfDto.Link;
                pdf.Description = pdfDto.Description;
                pdf.Name = pdfDto.Name;
                pdf.LastUpdated = DateTime.Now;
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("updating pdfs"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var pdf = await _context.Pdfs.FindAsync(id);
                if (pdf == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("pdf"));
                _context.Pdfs.Remove(pdf);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("deleting pdf"));
            }
        }
    }
}
