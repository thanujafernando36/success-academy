﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SuccessAcademy.Authentication;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SuccessAcademy.GlobalMethods;
using SuccessAcademy.Models;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Route("api/v1/user")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;
        private readonly AcademyContext _context;

        public AuthenticateController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, AcademyContext context) 
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model) 
        {
            try 
            {
                var user = await userManager.FindByNameAsync(model.Username);
                if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
                {
                    var userRoles = await userManager.GetRolesAsync(user);
                    var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                    foreach (var userRole in userRoles)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                    }

                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                    var token = new JwtSecurityToken(
                        issuer: _configuration["JWT:ValidIssuer"],
                        audience: _configuration["JWT:ValidAudience"],
                        expires: DateTime.Now.AddHours(3),
                        claims: authClaims,
                        signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                        );

                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo
                    });
                }
                return Ok(CustomHttpResponse.unauthorizedErrorResponse());
            } 
            catch 
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("logging user in"));
            }
            
        }

        [HttpPost]
        [Route("reigster/student")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            try 
            {
                var userExists = await userManager.FindByNameAsync(model.Username);
                if (userExists != null)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("registering student due to Username being already taken"));

                User user = new User()
                {
                    Name = model.Name,
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Username,
                    Dob = model.Dob,
                    Grade = model.Grade,
                    JoinedDate = model.JoinedDate,
                    CreatedDate = DateTime.Now,
                    LastUpdated = DateTime.Now,
                };
                var result = await userManager.CreateAsync(user, model.Password);
                if (!result.Succeeded)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("registering student, check user details"));

                if (!await roleManager.RoleExistsAsync(UserRoles.Student))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Student));

                if (await roleManager.RoleExistsAsync(UserRoles.Student))
                {
                    await userManager.AddToRoleAsync(user, UserRoles.Student);
                }
                if (model.Classes != null) 
                {
                    var classes = model.Classes.Split(',');
                    foreach (var classId in classes)
                    {
                        try
                        {
                            var classRoom = _context.ClassRooms.Find(int.Parse(classId));
                            if (classRoom == null)
                                throw new Exception();
                            var studentClass = new StudentClass
                            {
                                User = user,
                                StudentClassRoom = classRoom
                            };
                            _context.StudentClass.Add(studentClass);
                        }
                        catch
                        {
                            _context.Users.Remove(user);
                            await _context.SaveChangesAsync();
                            return Ok(CustomHttpResponse.internalServerErrorResponse("registering student, check student's classes"));
                        }
                    }
                }
                
                await _context.SaveChangesAsync();
                return Ok(new Response { Status = "Success", Message = "User created successfully!" });
            }
            catch 
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("registering student"));
            }
            
        }

        [HttpPost, Route("reigster/admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model) 
        {
            try 
            {
                var userExists = await userManager.FindByNameAsync(model.Username);
                if (userExists != null)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("registering user due to Username being already taken"));

                User user = new User()
                {
                    Name = model.Name,
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Username,
                    Dob = model.Dob,
                    Grade = model.Grade,
                    JoinedDate = model.JoinedDate,
                    CreatedDate = DateTime.Now,
                    LastUpdated = DateTime.Now,
                };
                var result = await userManager.CreateAsync(user, model.Password);
                if (!result.Succeeded)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("registering user, check user details"));
                if (!await roleManager.RoleExistsAsync(UserRoles.Admin))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
                if (!await roleManager.RoleExistsAsync(UserRoles.Student))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Student));

                if (await roleManager.RoleExistsAsync(UserRoles.Admin))
                {
                    await userManager.AddToRoleAsync(user, UserRoles.Admin);
                }

                return Ok(new Response { Status = "Success", Message = "Admin created successfully!" });
            } 
            catch 
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("registering admin"));
            }
            
        }
    }
}
