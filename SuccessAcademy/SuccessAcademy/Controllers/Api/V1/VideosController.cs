﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuccessAcademy.Authentication;
using SuccessAcademy.Dtos;
using SuccessAcademy.GlobalMethods;
using SuccessAcademy.Models;
using SuccessAcademy.ViewModels;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class VideosController : ControllerBase
    {
        private readonly AcademyContext _context;

        public VideosController(AcademyContext context)
        {
            _context = context;
        }

        private VideoViewmodel MapVideoViewModel(Video video) 
        {
            if (video.ClassRoom == null || video.Subject == null)
                throw new Exception();
            var videoViewmodel = new VideoViewmodel
            {
                Id = video.Id,
                Name = video.Name,
                CreatedDate = video.CreatedDate,
                Description = video.Description,
                ClassRoomName = video.ClassRoom.Name,
                SubjectName = video.Subject.Name,
                Link = video.Link,
                Grade = video.ClassRoom.Grade,
                LastUpdated = video.LastUpdated
            };
            return videoViewmodel;
        }

        // GET: api/Videos
        [HttpGet]
        public async Task<IActionResult> GetVideos()
        {
            try 
            {
                var videos = await _context.Videos.ToListAsync();
                if (videos == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("videos"));
                var videoViewmodels = new List<VideoViewmodel>();
                foreach (Video video in videos) 
                {
                    var videoViewmodel = MapVideoViewModel(video);
                    videoViewmodels.Add(videoViewmodel);
                }
                return Ok(videoViewmodels);
            }
            catch 
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving videos"));
            }
        }

        // GET: api/Videos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVideo(int id)
        {
            try
            {
                var video = await _context.Videos.FindAsync(id);
                if (video == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("video"));
                var videoViewmodel = MapVideoViewModel(video);
                return Ok(videoViewmodel);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving videos"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreateVideo([FromBody] VideoDto videoDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating video"));
                var videoClass = _context.ClassRooms.Find(videoDto.ClassId);
                var videoSubject = _context.Subjects.Find(videoDto.ClassId);
                var video = new Video 
                {
                    Name = videoDto.Name,
                    Description = videoDto.Description,
                    Link = videoDto.Link,
                    LastUpdated = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    ClassRoom = videoClass,
                    Subject = videoSubject
                };
                _context.Videos.Add(video);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("creating video"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVideo(int id, [FromBody] VideoDto videoDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating video"));
                var video = await _context.Videos.FindAsync(id);
                if (video == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("video"));
                video.Link = videoDto.Link;
                video.Description = videoDto.Description;
                video.Name = videoDto.Name;
                video.LastUpdated = DateTime.Now;
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("updating videos"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var video = await _context.Videos.FindAsync(id);
                if (video == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("video"));
                _context.Videos.Remove(video);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("deleting video"));
            }
        }
    }
}
