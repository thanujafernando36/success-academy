﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuccessAcademy.Authentication;
using SuccessAcademy.Dtos;
using SuccessAcademy.GlobalMethods;
using SuccessAcademy.Models;
using SuccessAcademy.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassRoomsController : ControllerBase
    {
        private readonly AcademyContext _context;

        public ClassRoomsController(AcademyContext context)
        {
            _context = context;
        }

        private ClassRoomViewmodel MapClassRoomViewModel(ClassRoom classRoom)
        {
            //if (classRoom.Subject == null)
            //    throw new Exception();
            var classRoomViewmodel = new ClassRoomViewmodel
            {
                Id = classRoom.Id,
                Name = classRoom.Name,
                Grade = classRoom.Grade,
                Language = classRoom.Language,
                StartTime = classRoom.StartTime,
                EndTime = classRoom.EndTime,
                SubjectName = classRoom.Subject.Name,
                CreatedDate = classRoom.CreatedDate,
                LastUpdated = classRoom.LastUpdated
            };
            return classRoomViewmodel;
        }

        private ClassRoomAdminViewModel MapClassRoomAdminViewModel(ClassRoom classRoom)
        {
            //if (classRoom.Subject == null)
            //    throw new Exception();
            var classRoomAdminViewmodel = new ClassRoomAdminViewModel
            {
                Id = classRoom.Id,
                Name = classRoom.Name,
                Grade = classRoom.Grade,
                Language = classRoom.Language,
                StartTime = classRoom.StartTime,
                EndTime = classRoom.EndTime,
                SubjectName = classRoom.Subject.Name,
                CreatedDate = classRoom.CreatedDate,
                LastUpdated = classRoom.LastUpdated
            };
            return classRoomAdminViewmodel;
        }

        private ClassRoom MapClassRoomDto(ClassRoomDto classRoomDto, Subject subject) 
        {
            var classRoom = new ClassRoom
            {
                Name = classRoomDto.Name,
                Grade = classRoomDto.Grade,
                Language = classRoomDto.Language,
                StartTime = classRoomDto.StartTime,
                EndTime = classRoomDto.EndTime,
                CreatedDate = DateTime.Now,
                LastUpdated = DateTime.Now,
                Subject = subject
            };
            return classRoom;
        }

        // GET: api/ClassRooms
        [HttpGet]
        public async Task<IActionResult> GetClassRooms()
        {
            try
            {
                var classRooms = await _context.ClassRooms.Include(c => c.Subject).ToListAsync();
                if (classRooms == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("classRooms"));
                var classRoomViewmodels = new List<ClassRoomViewmodel>();
                foreach (ClassRoom classRoom in classRooms)
                {
                    var classRoomViewmodel = MapClassRoomViewModel(classRoom);
                    classRoomViewmodels.Add(classRoomViewmodel);
                }
                return Ok(classRoomViewmodels);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving classRooms"));
            }
        }

        // GET: api/ClassRooms
        [HttpGet("admin")]
        public async Task<IActionResult> GetClassRoomsForAdmin()
        {
            try
            {
                var classRooms = await _context.ClassRooms.Include(c => c.Subject).ToListAsync();
                if (classRooms == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("classRooms"));
                var classRoomAdminViewmodels = new List<ClassRoomAdminViewModel>();
                foreach (ClassRoom classRoom in classRooms)
                {
                    var classRoomAdminViewmodel = MapClassRoomAdminViewModel(classRoom);
                    classRoomAdminViewmodels.Add(classRoomAdminViewmodel);
                }
                return Ok(classRoomAdminViewmodels);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving classRooms for admin"));
            }
        }

        // GET: api/ClassRooms/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClassRoom(int id)
        {
            try
            {
                var classRoom = await _context.ClassRooms.Include(c => c.Subject).FirstOrDefaultAsync(c => c.Id == id);
                if (classRoom == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("classRoom"));
                var classRoomViewmodel = MapClassRoomViewModel(classRoom);
                return Ok(classRoomViewmodel);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving classRoom"));
            }
        }

        //[Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreateClassRoom([FromBody] ClassRoomDto classRoomDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating classRoom"));
                var classRoomSubject = _context.Subjects.Find(classRoomDto.SubjectId);
                if (classRoomSubject == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("Subject"));
                var classRoom = MapClassRoomDto(classRoomDto, classRoomSubject);
                _context.ClassRooms.Add(classRoom);
                await _context.SaveChangesAsync();
                return Ok(classRoom);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("creating classRoom"));
            }
        }

        //[Authorize(Roles = UserRoles.Admin)]
        // PUT: api/ClassRooms/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClassRoom(int id, [FromBody] ClassRoomDto classRoomDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating classRoom"));
                var classRoom = await _context.ClassRooms.FindAsync(id);
                if (classRoom == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("classRoom"));
                var classRoomSubject = _context.Subjects.Find(classRoomDto.SubjectId);
                if (classRoomSubject == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("Subject"));
                classRoom.Grade = classRoomDto.Grade;
                classRoom.Name = classRoomDto.Name;
                classRoom.LastUpdated = DateTime.Now;
                classRoom.StartTime = classRoomDto.StartTime;
                classRoom.EndTime = classRoomDto.EndTime;
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("updating classRooms"));
            }
        }

        //[Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var classRoom = await _context.ClassRooms.FindAsync(id);
                if (classRoom == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("classRoom"));
                _context.ClassRooms.Remove(classRoom);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("deleting classRoom"));
            }
        }
    }
}
