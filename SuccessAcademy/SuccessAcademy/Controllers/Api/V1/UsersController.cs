﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SuccessAcademy.Authentication;
using SuccessAcademy.Dtos;
using SuccessAcademy.GlobalMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;
        private readonly AcademyContext _context;

        public UsersController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, AcademyContext context)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
            _context = context;
        }

        private UserDto mapUserObject(User user) 
        {
            var userDto = new UserDto 
            {
                Name = user.Name,
                Dob = user.Dob,
                JoinedDate = user.JoinedDate,
                CreatedDate = user.CreatedDate,
                ProfilePic = user.ProfilePic,
                LastUpdated = user.LastUpdated,
                Grade = user.Grade,
                UserName = user.UserName
            };
            return userDto;
        }

        [HttpGet]
        [Route("{userName}")]
        public async Task<IActionResult> GetUser(string userName) 
        {
            try
            {
                var user = await userManager.FindByNameAsync(userName);
                if (user == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("User"));
                var userDto = mapUserObject(user);
                return Ok(userDto);
            }
            catch 
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("Retrieving user data"));
            }
        }

        [HttpGet]
        public IActionResult GetUsers()
        {
            try
            {
                var users = _context.Users.ToList();
                if (users == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("Users"));
                var userDtos = new List<UserDto>();
                foreach (User user in users)
                {
                    var userDto = mapUserObject(user);
                    userDtos.Add(userDto);
                }

                return Ok(userDtos);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("Retrieving user data"));
            }
        }

        [Authorize]
        [HttpPut]
        [Route("{userName}")]
        public async Task<IActionResult> UpdateUser(string userName, [FromBody] UserDto userDto)
        {
            try
            {
                var user = await userManager.FindByNameAsync(userName);
                if (user == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("User"));
                user.Dob = userDto.Dob;
                user.Name = userDto.Name;
                user.LastUpdated = DateTime.Now;
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("updating user"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("{userName}")]
        public async Task<IActionResult> DeleteUser(string userName) 
        {
            try 
            {
                var user = await userManager.FindByNameAsync(userName);
                if (user == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("User"));
                await userManager.DeleteAsync(user);
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("deleting user"));
            }
        }

    }
}
