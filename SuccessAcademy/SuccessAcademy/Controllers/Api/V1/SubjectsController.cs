﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuccessAcademy.Authentication;
using SuccessAcademy.Dtos;
using SuccessAcademy.GlobalMethods;
using SuccessAcademy.Models;
using SuccessAcademy.ViewModels;

namespace SuccessAcademy.Controllers.Api.V1
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly AcademyContext _context;

        public SubjectsController(AcademyContext context)
        {
            _context = context;
        }

        private SubjectViewModel MapSubjectViewModel(Subject subject)
        {
            var subjectViewModel = new SubjectViewModel
            {
                Id = subject.Id,
                Name = subject.Name,
                CreatedDate = subject.CreatedDate,
                LastUpdated = subject.LastUpdated
            };
            return subjectViewModel;
        }

        // GET: api/Subjects
        [HttpGet]
        public async Task<IActionResult> GetSubjects()
        {
            try
            {
                var subjects = await _context.Subjects.ToListAsync();
                if (subjects == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("subjects"));
                var subjectViewModels = new List<SubjectViewModel>();
                foreach (Subject subject in subjects)
                {
                    var subjectViewModel = MapSubjectViewModel(subject);
                    subjectViewModels.Add(subjectViewModel);
                }
                return Ok(subjectViewModels);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving subjects"));
            }
        }

        // GET: api/Subjects/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubject(int id)
        {
            try
            {
                var subject = await _context.Subjects.FindAsync(id);
                if (subject == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("subject"));
                var subjectViewModel = MapSubjectViewModel(subject);
                return Ok(subjectViewModel);
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("retrieving subjects"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public async Task<IActionResult> CreateSubject([FromBody] SubjectDto subjectDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating subject"));
                var subject = new Subject
                {
                    Name = subjectDto.Name,
                    LastUpdated = DateTime.Now,
                    CreatedDate = DateTime.Now
                };
                _context.Subjects.Add(subject);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("creating subject"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject(int id, [FromBody] SubjectDto subjectDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Ok(CustomHttpResponse.internalServerErrorResponse("creating subject"));
                var subject = await _context.Subjects.FindAsync(id);
                if (subject == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("subject"));
                subject.Name = subjectDto.Name;
                subject.LastUpdated = DateTime.Now;
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("updating subjects"));
            }
        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var subject = await _context.Subjects.FindAsync(id);
                if (subject == null)
                    return Ok(CustomHttpResponse.objectNotFoundResponse("subject"));
                _context.Subjects.Remove(subject);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return Ok(CustomHttpResponse.internalServerErrorResponse("deleting subject"));
            }
        }
    }
}
