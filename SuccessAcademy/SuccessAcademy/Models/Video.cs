﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Models
{
    public class Video
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public string Link { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }
        [Required]
        public ClassRoom ClassRoom { get; set; }
        [Required]
        public Subject Subject { get; set; }
    }
}
