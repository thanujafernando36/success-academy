﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Models
{
    public class Subject
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }
    }
}
