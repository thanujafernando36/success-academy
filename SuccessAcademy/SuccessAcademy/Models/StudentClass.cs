﻿using SuccessAcademy.Authentication;

namespace SuccessAcademy.Models
{
    public class StudentClass
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int StudentClassRoomId { get; set; }
        public ClassRoom StudentClassRoom { get; set; }
    }
}
