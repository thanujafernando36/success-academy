﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Models
{
    public class ClassRoom
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Grade { get; set; }

        public string Language { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public Subject Subject { get; set; }

        public IList<StudentClass> StudentClasses { get; set; }
    }
}
