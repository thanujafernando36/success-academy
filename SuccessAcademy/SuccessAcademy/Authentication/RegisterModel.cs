﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Authentication
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        public int Grade { get; set; }

        public string Classes { get; set; }

        public DateTime JoinedDate { get; set; }

        public DateTime Dob { get; set; }

    }
}
