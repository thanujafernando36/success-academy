﻿using Microsoft.AspNetCore.Identity;
using SuccessAcademy.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuccessAcademy.Authentication
{
    public class User : IdentityUser
    {
        public int Grade { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Dob { get; set; }
        public DateTime JoinedDate { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }
        public string ProfilePic { get; set; }
        public IList<StudentClass> StudentClasses { get; set; }
    }
}
