﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SuccessAcademy.Models;

namespace SuccessAcademy.Authentication
{
    public class AcademyContext : IdentityDbContext<User>
    {
        public AcademyContext(DbContextOptions<AcademyContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<StudentClass>().HasKey(sc => new {sc.UserId, sc.StudentClassRoomId});
        }
        public DbSet<ClassRoom> ClassRooms { get; set; }
        public DbSet<Pdf> Pdfs { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentClass> StudentClass { get; set; }
    }
}
